<?php

namespace PHPComposter\PHPComposter_PHPCS_Drupal;

use PHP_CodeSniffer\Runner;
use PHP_CodeSniffer\Config;
use PHP_CodeSniffer\Reporter;
use PHP_CodeSniffer\Files\DummyFile;
use PHPComposter\PHPComposter\BaseAction;

require_once dirname(__DIR__, 5).'/vendor/squizlabs/php_codesniffer/autoload.php';

/**
 * Provides the pre commit hook to run the sniffs.
 */
class Sniffer extends BaseAction
{

    /**
     * Run PHP Code Sniffer over PHP files as pre-commit hook.
     */
    public function preCommit()
    {

        $files = $this->getStagedFiles(
            "'/*.\(php\|module\|inc\|install\|test\|profile\|theme\|info\|txt\|md\)$'",
            false
        );
        if (empty($files)) {
            return;
        }

        $files = preg_grep("/(\/core\/|\/libraries\/|\/capistrano\/)/i", $files, PREG_GREP_INVERT);

        if (empty($files)) {
            return;
        }

          if (defined('PHP_CODESNIFFER_CBF') === false) {
            define('PHP_CODESNIFFER_CBF', true);
        }

        if (defined('PHP_CODESNIFFER_VERBOSITY') === false) {
            define('PHP_CODESNIFFER_VERBOSITY', false);
        }

        // If you don't want timing information in the report, remove this.
        //Timing::startTiming();

        //echo 'Running PHP CodeSniffer in ' . $this->root . PHP_EOL;

        @$config = $this->getExtraKey('php-composter-phpcs-drupal', [
          'standard' => 'Drupal'
        ]);



        $runner = new Runner();

        $runner->config = new Config(['dummy'], false);
        $runner->config->standards = array($config['standard']);
        $runner->init();

        // Hard-code some other config settings.
        // Do this after init() so these values override anything that was set in
        // the rulesets we processed during init(). Or do this before if you want
        // to use them like defaults instead.
        $runner->config->reports      = array('summary' => null, 'full' => null);
        $runner->config->verbosity    = 0;
        $runner->config->showProgress = false;
        $runner->config->interactive  = false;
        $runner->config->cache        = false;
        $runner->config->showSources  = true;

        // Create the reporter, using the hard-coded settings from above.
        $runner->reporter = new Reporter($runner->config);


foreach ( $files as $file_path ) {
  $file = new DummyFile( file_get_contents( $file_path ), $runner->ruleset, $runner->config );
  $file->path = $file_path;

  $runner->processFile( $file );
}

        if ($runner->reporter->totalErrors !== 0 || $runner->reporter->totalWarnings !== 0) {
            $runner->reporter->printReports();
            $this->error(
                    "PHP Code Sniffer found errors! Aborting Commit.",
                    1
                );
        }

$this->success(
                'PHP Code Sniffer',
                false
            );

    }
}
